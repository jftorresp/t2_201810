package model.data_structures.test;

import static org.junit.Assert.*;

import java.util.Date;

import org.junit.Before;
import org.junit.Test;

import model.data_structures.List;
import model.data_structures.Node;
import model.vo.Service;
import model.vo.Taxi;


public class NodeTest<T> 
{
	/**
	 *A new node to be used in the test.
	 */
	private Node node;

	/**
	 * a new service element to be in the node.
	 */
	private Service service;

	/**
	 * Creates a new Service and a node with it. This method should be used in every test.
	 */
	@Before
	public void setupEscenary( )
	{
		service = new Service("Cabify", "101101","1039434", 26, 31, 4.5, 4, new Taxi("1234", "Tappsi"));
		node = new Node<>(service);
	}

	/**
	 * Creates a new Service and a node with it. This method should be used in every test.
	 */
	@Before
	public void setupEscenary2( )
	{
		service = new Service("Uber", "101f1031","10394d34", 28, 21, 6.5, 7, new Taxi("1224", "Cab taxi"));
		node = new Node<>(service);
	}

	/**
	 * Test 1: Verifies method changeNext(). <br>
	 * <b>Methds to prove:</b> <br>
	 * Node<br>
	 * getNext<br>
	 * changeNext<br>
	 * <b> Cases: </b><br>
	 * 1) No next node. <br>
	 * 2) There is a next node.
	 */
	@Test
	public void testChangeNext( )
	{

		Service service2 = new Service("Cabify", "101101","1039434", 26, 31, 4.5, 4, new Taxi("1234", "Tappsi"));
		setupEscenary();
		// Case1
		Node node1 = new Node(service2);
		assertNull( "The next element should be null.", node.getNext( ) );
		node.changeNext(node1);
		assertNotNull( "The next element should exist.", node.getNext( ));
		assertEquals( "The next element doesn´t match.", node1.getElement(), node.getNext().getElement());


		// Case 2
		Service service3 = new Service("Uber", "101f1031","10394d34", 28, 21, 6.5, 7, new Taxi("1224", "Cab taxi"));
		setupEscenary2();
		Node node2 = new Node(service2);
		node.changeNext(node2);
		assertNotNull( "El elemento anterior debe existir pues ha sido asignado.", node.getNext());
		assertEquals( "El elemento anterior no coincide", node2.getElement(), node.getNext().getElement());
	}

	/**
	 * Test 2: Verifies method hasNext(). <br>
	 * <b>Methods to prove:</b> <br>
	 * Node<br>
	 * hasNext<br>
	 * <b> Cases: </b><br>
	 * 1) No next node. <br>
	 * 2) There is a next node.
	 */
	@Test
	public void testHasNext()
	{
		setupEscenary();
		List list1 = new List<>();
		list1.add(service);
		assertFalse("The node does not have a next", node.hasNext());

		setupEscenary2();
		Service service2 = new Service("Cabify", "101101","1039434", 26, 31, 4.5, 4, new Taxi("1234", "Tappsi"));
		Node node2 = new Node(service2);
		List list2 = new List<>();
		list2.add(service);
		list2.add(service2);
		assertFalse("The node does not have a next", node.hasNext());
	}
}
