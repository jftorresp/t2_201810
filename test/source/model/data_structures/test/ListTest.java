package model.data_structures.test;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import model.data_structures.List;
import model.data_structures.Node;
import model.vo.Service;
import model.vo.Taxi;
public class ListTest 
{
	private List list;

	// -----------------------------------------------------------------
	// Escenaries
	// -----------------------------------------------------------------
	/**
	 * Creates a new empty list. This method executes before every test.
	 */
	@Before
	public void setupEscenary1( )
	{
		list = new List<>();

	}

	/**
	 * Creates a new list with one taxi. This method executes before every test.
	 */
	public void setupEscenary2( )
	{
		list = new List<>();
		Taxi element= new Taxi("123", "Compañia del sabor");
		list.add(element);
	}

	/**
	 * Creates a new list with one taxi. This method executes before every test.
	 */
	public void setupEscenary3( )
	{
		list = new List<>();
		list.size();
	}

	/**
	 * Creates a new list with one taxi. This method executes before every test.
	 */
	public void setupEscenary4( )
	{
		list = new List<>();
		Taxi element1= new Taxi("123", "Compañia del sabor");
		Taxi element2= new Taxi("1234", "Compañia de taxis");
		Taxi element3= new Taxi("1235", "Compañia cabify");
		Taxi element4= new Taxi("1236", "Compañia tappsi");
		Taxi element5= new Taxi("1237", "Compañia uber");
		list.add(element1);
		list.add(element2);
		list.add(element3);
		list.add(element4);
		list.add(element5);
		list.size();
	}


	/**
	 * Test 1: Verifies method add(). <br>
	 * <b>Methods to prove:</b> <br>
	 * List<br>
	 * add<br>
	 * get<br>
	 */
	@Test
	public void testAdd()
	{
		Taxi element= new Taxi("123", "Compañia del sabor");
		setupEscenary1();
		try{
			list.add(element);
			assertNotNull("Se deberia agregar la banda",list.get(element));
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}

	/**
	 * Test 2: Verifies method delete(). <br>
	 * <b>Methods to prove:</b> <br>
	 * List<br>
	 * getCurrent<br>
	 * size<br>
	 * delete<br>
	 */
	@Test
	public void testDelete()
	{
		setupEscenary2();
		try
		{
			list.delete(list.getCurrent());
			assertEquals("El tamaño de la lista deberia ser 0",0,list.size());
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}

	/**
	 * Test 3: Verifies method get(). <br>
	 * <b>Methods to prove:</b> <br>
	 * List<br>
	 * get<br>
	 */
	@Test 
	public void testGet()
	{
		Taxi element2= new Taxi("123", "Compañia del sabor");
		setupEscenary2();
		try 
		{
			list.get(element2);
			assertEquals("No es el elemento esperado",element2 ,list.get(element2));
		} catch (Exception e) 
		{
			// TODO: handle exception
		}
	}

	/**
	 * Test 3: Verifies method size(). <br>
	 * <b>Methods to prove:</b> <br>
	 * List<br>
	 * size<br>
	 * <b> Cases: </b><br>
	 * 1) Size is 0. <br>
	 * 2) Size is 5.
	 */
	@Test
	public void testSize()
	{
		setupEscenary3( );
		assertTrue( "The size should be 0", list.size() == 0);

		setupEscenary4();
		assertTrue( "The size should be 5", list.size() == 5 );	
	}

	/**
	 * Test 3: Verifies method get(int). <br>
	 * <b>Methods to prove:</b> <br>
	 * List<br>
	 * get<br>
	 * add<br>
	 */
	@Test
	public void testGetByPos()
	{
		Taxi element2= new Taxi("123", "Compañia del sabor");
		list.add(element2);
		setupEscenary2();
		try 
		{
			list.get(0);
			assertEquals("Is not the expected element",list.get(0) ,element2);
		} catch (Exception e) 
		{
			// TODO: handle exception
		}
	}
}


