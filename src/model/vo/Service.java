package model.vo;

/**
 * Representation of a Service object
 */
public class Service implements Comparable<Service> {


	private Taxi taxi;
	
	/**
	 * Atribute with the id of the trip in the Service.
	 */
	private String trip_Id;
	
	/**
	 * Atribute with the name of the company of the Taxi service.
	 */
	private String company;
	
	/**
	 * Atribute with the id of the taxi in the Service.
	 */
	private String taxi_id;
	
	/**
	 * Atribute with the time of the trip in seconds in the Service.
	 */
	private int trip_Seconds;
	
	/**
	 * Atribute with the distance of the trip in miles in the Service.
	 */
	private double trip_Miles;
	
	/**
	 * Atribute with the total cost of the trip in the Service.
	 */
	private double trip_Total;
	
	/**
	 * Atribute with the pickup community areas of the trip in the Service.
	 */
	private int pickup_community_areas;
	
	/**
	 * Constructs a new Taxi Service with diferent atributes.
	 * @param pCompany taxi company in the service.
	 * @param pTaxi id of the taxi in the service.
	 * @param pTrip_Id id of the service.
	 * @param pTrip_Seconds time in seconds of the trip.
	 * @param pTrip_Miles distance of the trip in miles.
	 * @param pTrip_Total total cost of the trip.
	 * @param pPickUp pickup community area of thetrip in the service.
	 */
	public Service(String pCompany, String pTaxiid, String pTrip_Id, int pTrip_Seconds, double pTrip_Miles, double pTrip_Total, int pPickUp, Taxi pTaxi)
	{
		company = pCompany;
		taxi_id = pTaxiid;
		trip_Id = pTrip_Id;
		trip_Seconds = pTrip_Seconds;
		trip_Miles = pTrip_Miles;
		trip_Total = pTrip_Total;
		pickup_community_areas = pPickUp;
		taxi = pTaxi;
	}
	
	/**
	 * @return id - Trip_id
	 */
	public String getTripId() 
	{
		return "trip Id";
	}	
	
	/**
	 * @return id - Taxi_id
	 */
	public String getTaxiId() 
	{
		return "taxi Id";
	}	
	
	/**
	 * @return time - Time of the trip in seconds.
	 */
	public int getTripSeconds() 
	{
		return 0;
	}

	/**
	 * @return miles - Distance of the trip in miles.
	 */
	public double getTripMiles() 
	{
		return 0;
	}
	
	/**
	 * @return total - Total cost of the trip
	 */
	public double getTripTotal() {
		// TODO Auto-generated method stub
		return 0;
	}
	
	public Taxi getTaxi()
	{
		return taxi;
	}
	
	public int pickUp()
	{
		return pickup_community_areas;
	}

	@Override
	public int compareTo(Service o) {
		// TODO Auto-generated method stub
		return 0;
	}
}
