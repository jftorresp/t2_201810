package model.data_structures;

/**
 * Abstract Data Type for a linked list of generic objects
 * This ADT should contain the basic operations to manage a list
 * add: add a new element T 
 * delete: delete the given element T 
 * get: get the given element T (null if it doesn't exist in the list)
 * size: return the number of elements
 * get: get an element T by position (the first position has the value 0) 
 * listing: set the listing of elements at the firt element
 * getCurrent: return the current element T in the listing (return null if it doesn�t exists)
 * next: advance to next element in the listing (return if it exists)
 * @param <T>
 */
public interface LinkedList <T extends Comparable<T>>  
{

	/**
	 * Adds an element T to the linked list.
	 * @param element An element T. element != null.
	 * @return true if the element was added or false if not.
	 */
	public boolean add(T element);

	/**
	 * Deletes an element T to the linked list.
	 * @param element An element T. element != null.
	 * @return true if the element was deleted or false if not.
	 */
	public boolean delete(T element);

	/**
	 * Gets an element T given from the linked list.
	 * @param T element of the list.
	 * @return The element T or null if it doesn´t exists in the list.
	 */
	public T get(T element);

	/**
	 * Returns the size of all the elements in the linked list.
	 * @param element An element T in the list. element != null.
	 * @return the number of elements in the linked list.
	 */
	public int size();

	/**
	 * Gets an element T given from the linked list by position.
	 * @param The position of the element. First position is 0
	 * @return The element T given by position or null if it doesn´t exists in the list.
	 */
	public T get(int position);
	
	/**
	 * Set the listing of elements at the firt element.
	 */
	public T[] listing(T[] arrayList);
	
	/**
	 * Gets the current element T in the linked list.
	 * @return The current element T given or null if it doesn´t exists in the list..
	 */
	public T getCurrent();
	
	/**
	 * Advance to next element in the listing.
	 * @return true if the element has a next or false if not.
	 */
	public boolean next();

}
