package model.data_structures;

public class Node<T extends Comparable<T>>
{
	//------------------------------------------------
	//Atributes
	//------------------------------------------------
	
	/**
	 * The next node of the actual node in the list.
	 */
	private Node next;
	
	/**
	 * The element in the node.
	 */
	private T element;
	
	//------------------------------------------------
	//Constructor
	//------------------------------------------------
	
	/**
	 * Constructs a node of a double linked list. <br>
	 * The next and previous node start as null.
	 */
	public Node(T pElement)
	{
		next = null;
		element = pElement;
	}
	
	//------------------------------------------------
	//Methods
	//------------------------------------------------
	
	/**
	 * Returns the element in the list.
	 * @return the element.
	 */
	public T getElement()
	{
		return element;
	}
	
	/**
	 * Returns the next node in the list.
	 * @return the next node.
	 */
	public Node<T> getNext()
	{
		return next;
	}
	
	/**
     * Change the next node to the node given by parameter. <br>
     * <b> post: </b> The next node was changed.
     * @param pNext Node to be asigned as next.
     */
	public void changeNext(Node<T> pNext)
	{
		next = pNext;
	}
		
	public boolean hasNext()
	{
		if(next != null)
		{
			return true;
		}
		return false;
	}
	
}
