package model.data_structures;


public class List <T extends Comparable<T>> implements LinkedList<T>
{
	//------------------------------------------------
	//Atributes
	//------------------------------------------------

	/**
	 * First node of the list.
	 */
	private Node<T> first;

	/**
	 * Actual node of the list.
	 */
	private Node<T> actual;

	/**
	 * Size of the list.
	 */
	private int size;

	//------------------------------------------------
	//Constructor
	//------------------------------------------------

	/**
	 * Constructs a new list.
	 * First and actual node start as null, size is 0.
	 */
	public List()
	{
		first = null;
		actual = null;
		size = 0;
	}

	//------------------------------------------------
	//Methods
	//------------------------------------------------
	
	/**
	 * Adds a new element T to the list.
	 * @param element of generic type T. T !=null T != "".
	 * @return true if the element was added to the list or false if not.
	 */
	@Override
	public boolean add(T element) 
	{
		boolean add = false;

		if ( first == null ) // Case 1: Empty List
		{ 
			first = new Node<T>(element);
			actual = first;
			size++;
			add = true;
		}
		else // Case 2: Not empty list
		{
			Node<T> actual = first;
			while(actual.hasNext())
			{
				actual = actual.getNext();
			}
			actual.changeNext(new Node<T>(element));	
			size++;
		}
		return add;
	}

	/**
	 * Deletes an element T of the list.
	 * @param element of generic type T. T !=null T != "".
	 * @return true if the element was deleted or false if not.
	 */
	@Override
	public boolean delete(T element) 
	{
		boolean delete = false;
		if(first.getElement().equals(element))
		{
			first = first.getNext();
			size--;
			delete = true;
		}
		else
		{
			Node<T> actual = first;
			while(actual.hasNext())
			{
				Node<T> next = actual.getNext();
				if(next.getElement().equals(element))
				{
					actual.changeNext(next.getNext());
					size--;
					delete = true;
				}
			}
		}
		return delete;
	}

	/**
	 * Gets an element T in the list.
	 * @param element of generic type T. T !=null T != "".
	 * @return the element T of the list.
	 */
	@Override
	public T get(T element) 
	{
		if(first != null)
		{
			Node<T> actual = first;
			if(actual.getElement().compareTo(element) == 0) return element;
			while(actual.hasNext())
			{
				actual = actual.getNext();
			}
		}
		return null;
	}

	/**
	 * Gets the size of the list.
	 * @return the size of the list.
	 */
	@Override
	public int size() 
	{
		return size;
	}

	/**
	 * Set the listing of elements ath the first.
	 * @return an array of T elements listed started by the first element.
	 */
	@Override
	public T[] listing(T[] arrayList) 
	{
		Node<T> actual = first;
		int count = 0;
		if(actual != null)
		{
			arrayList[count] = actual.getElement();
			count++;
		}
		while(actual.hasNext())
		{
			actual = actual.getNext();
			arrayList[count] = actual.getElement();
			count++;
		}
		return arrayList;
	}

	/**
	 * Gets the current element T in the list.
	 * @return the current element T of the list.
	 */
	@Override
	public T getCurrent() 
	{
		return actual.getElement();
	}

	@Override
	public boolean next() 
	{
		boolean next = false;
		actual = actual.getNext();
		if(actual == null)
		{
			next = false;
		}
		else
		{
			next = true;
		}
		return next;
	}

	/**
	 * Gets an element T in the list by its position.
	 * @param postion of the element T in the list. T !=null T != "".
	 * @return the element T given by position.
	 * @throws IndexOutOfBoundsException if the position its not within the size of the list.
	 */
	@Override
	public T get(int position) throws IndexOutOfBoundsException
	{
		if(position >= size)
		{
			throw new IndexOutOfBoundsException("The position is not correct");
		}
		else
		{
			Node<T> actual = first;
			while(position >= 0)
			{
				actual = actual.getNext();
				position--;
			}
			return actual.getElement();
		}
	}

	/**
	 * Gets a Node in the list by its position.
	 * @param position of the node in the list with parameter of generic type T. T !=null T != "".
	 * @return the Node given by position.
	 * @throws IndexOutOfBoundsException if the position its not within the size of the list.
	 */
	public Node<T> get1(int position) throws IndexOutOfBoundsException
	{
		if(position >= size)
		{
			throw new IndexOutOfBoundsException("The position is not correct");
		}
		else
		{
			Node<T> actual = first;
			while(position >= 0)
			{
				actual = actual.getNext();
				position--;
			}
			return actual;
		}
	}
}
