package model.logic;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.InputStreamReader;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonIOException;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.JsonSyntaxException;
import com.google.gson.stream.JsonReader;

import api.ITaxiTripsManager;
import model.vo.Taxi;
import model.vo.Service;
import model.data_structures.LinkedList;
import model.data_structures.List;
import model.data_structures.Node;

public class TaxiTripsManager implements ITaxiTripsManager 
{

	private List<Service> list;

	private Taxi taxi;

	// TODO
	// Definition of data model 

	public void loadServices (String serviceFile) 
	{
		list = new List<Service>();

		System.out.println("Inside loadServices with" + serviceFile);

		JsonParser parser = new JsonParser();

		try {
			serviceFile = "./data/taxi-trips-wrvz-psew-subset-small.json";

			/* Cargar todos los JsonObject (servicio) definidos en un JsonArray en el archivo */
			JsonArray arr= (JsonArray) parser.parse(new FileReader(serviceFile));

			/* Tratar cada JsonObject (Servicio taxi) del JsonArray */
			for (int i = 0; arr != null && i < arr.size(); i++)
			{
				JsonObject obj= (JsonObject) arr.get(i);

				if(obj == null)
				{
					System.out.println("The object is null in:" + i);
				}

				/* Mostrar un JsonObject (Servicio taxi) */
				System.out.println("------------------------------------------------------------------------------------------------");
				System.out.println(obj);

				/* Obtener la propiedad company de un servicio (String)*/
				String company = "N/A";
				if(obj.get("company") != null)
				{ company = obj.get("company").getAsString();	

				/* Obtener la propiedad dropoff_census_tract de un servicio (String)*/
				String dropoff_census_tract = "N/A";
				if ( obj.get("dropoff_census_tract") != null )
				{ dropoff_census_tract = obj.get("dropoff_census_tract").getAsString(); }

				/* Obtener la propiedad dropoff_centroid_latitude de un servicio (String)*/
				String dropoff_centroid_latitude = "NaN";
				if ( obj.get("dropoff_centroid_latitude") != null )
				{ dropoff_centroid_latitude = obj.get("dropoff_centroid_latitude").getAsString(); }

				/* Obtener la propiedad dropoff_centroid_longitude de un servicio (String) */
				String dropoff_centroid_longitude = "NaN";
				if ( obj.get("dropoff_centroid_longitude") != null )
				{ dropoff_centroid_longitude = obj.get("dropoff_centroid_longitude").getAsString(); }

				JsonObject dropoff_localization_obj = null; 

				// System.out.println("(Lon= "+dropoff_centroid_longitude+ ", Lat= "+dropoff_centroid_latitude +") (Datos String)" );

				/* Obtener la propiedad dropoff_centroid_location (JsonObject) de un servicio*/
				if ( obj.get("dropoff_centroid_location") != null )
				{ dropoff_localization_obj =obj.get("dropoff_centroid_location").getAsJsonObject();

				/* Obtener la propiedad coordinates (JsonArray) de la propiedad dropoff_centroid_location (JsonObject)*/
				JsonArray dropoff_localization_arr = dropoff_localization_obj.get("coordinates").getAsJsonArray();

				/* Obtener cada coordenada del JsonArray como Double */
				double longitude = dropoff_localization_arr.get(0).getAsDouble();
				double latitude = dropoff_localization_arr.get(1).getAsDouble();
				// System.out.println( "[Lon: " + longitude +", Lat:" + latitude + " ] (Datos double)");
				}
				else
				{
					// System.out.println( "[Lon: NaN, Lat: NaN ]");
				}

				/* Obtener la propiedad pickup_community_area de un servicio (int) */
				int pickup_community_area = 0;
				if ( obj.get("pickup_community_area") != null )
				{ pickup_community_area = obj.get("pickup_community_area").getAsInt(); }

				/* Obtener la propiedad taxi_id de un taxi (String) */
				String taxi_id = "N/A";
				if ( obj.get("taxi_id") != null )
				{ taxi_id = obj.get("taxi_id").getAsString(); }


				/* Obtener la propiedad trip_id de un servicio (String) */
				String trip_id = "N/A";
				if ( obj.get("trip_id") != null )
				{ trip_id = obj.get("trip_id").getAsString(); }

				/* Obtener la propiedad trip_miles de un servicio (double) */
				double trip_miles = 0;
				if ( obj.get("trip_miles") != null )
				{ trip_miles = obj.get("trip_miles").getAsDouble(); }

				/* Obtener la propiedad trip_seconds de un servicio (int) */
				int trip_seconds = 0;
				if ( obj.get("trip_seconds") != null )
				{ trip_seconds = obj.get("trip_seconds").getAsInt(); }

				/* Obtener la propiedad trip_total de un servicio (double) */
				double trip_total = 0;
				if ( obj.get("trip_miles") != null )
				{ trip_total = obj.get("trip_miles").getAsDouble(); }

				taxi = new Taxi(taxi_id, company);

				Service service = new Service(company, taxi_id, trip_id, trip_seconds, trip_miles, trip_total, pickup_community_area, taxi);
				list.add(service);

				}
			}
		}
		catch (JsonIOException e1 ) 
		{
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		catch (JsonSyntaxException e2) 
		{
			// TODO Auto-generated catch block
			e2.printStackTrace();
		}
		catch (FileNotFoundException e3) 
		{
			// TODO Auto-generated catch block
			e3.printStackTrace();	
		}

		System.out.println("Se han cargado exitosamente los datos!!!");
	}

	@Override
	public LinkedList<Taxi> getTaxisOfCompany(String company) 
	{
		int count = 0;
		LinkedList<Taxi> listTaxi = new List<>();
		if(company != null)
		{
			Node<Service> actual = list.get1(0);
			while(actual.hasNext())
			{
				if(actual.getElement().getTaxi().getCompany().compareTo(company) == 0)
				{
					listTaxi.add(actual.getElement().getTaxi());	
				}
				count = count + 1;
				actual = list.get1(0).getNext();
			}	
		}
		//		System.out.println("Inside getTaxisOfCompany with " + company);
		return listTaxi;
	}

	@Override
	public LinkedList<Service> getTaxiServicesToCommunityArea(int communityArea) 
	{
		int count = 0;
		LinkedList<Service> listService = new List<>();
		if(communityArea != 0)
		{
			Node<Service> actual = list.get1(0);
			while(actual.hasNext())
			{
				if(actual.getElement().pickUp() == communityArea)
				{
					listService.add(actual.getElement());
					count = count + 1;
				}
				actual = list.get1(0).getNext();
			}
		}

		System.out.println("Inside getTaxiServicesToCommunityArea with " + communityArea);
		return null;
	}


}
